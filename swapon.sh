#!/bin/bash
swapon -s
if [[ $(swapon -s) ]]; then
    echo "swap is OK"
else
    echo "no swap found. Creating swap..."
    sudo swapoff -a
    sudo fallocate -l 2G /var/swap.img
    sudo chmod 600 /var/swap.img
    sudo mkswap /var/swap.img
    sudo swapon /var/swap.img
    sudo echo "/var/swap.img    none    swap    sw    0    0" >> /etc/fstab
    free
    echo "swap is now OK"
fi
